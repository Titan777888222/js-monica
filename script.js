
class User {
  
  constructor(firstName, lastName, gender) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
  }
 
  displayDetails() {
	  
    // todo: Add implementation (реализация)
    // Return a string 
    return ""; 
  }
};
// To do:
// Show my details`My name is <first name> <last name> and I'm a <gender>`

const button = document.getElementById('button');
const details = document.getElementById('details');

button.addEventListener("click", function() {
  // To do:
  // create an object User - tip: const user = new User(firstName, lastName, gender)
  // In the <div id="details"> display data from the form      //https://www.mvcode.com/lessons/display-form-results-with-javascript
  // tip: get string with details from user.displayDetails()
});

// Tips:
// To get the gender value. You will need to find checked radio button 
// Knowledge base
// Button type radio
// https://www.w3schools.com/jsref/prop_radio_checked.asp
// Object creation
// https://www.w3schools.com/js/js_objects.asp	
// https://www.digitalocean.com/community/tutorials/understanding-classes-in-javascript //https://learn.javascript.ru/class